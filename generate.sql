SPOOL ./reset.sql

SELECT
   'ALTER USER ' || owner ||' IDENTIFIED BY ' || owner || ';' as tmp
FROM
    schema_version_registry
WHERE
    mrc_name = 'DEV';

SPOOL off